# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repository contains scilab files and data for a numerical modelof magma ocean crystallization
### How do I get set up? ###

* To run the simulations, you will need scilab installed in your computer. The file hmh.sce is the central file for creating data. It's major dependency is on module.sce.
* To generate figures, run post_process_MO.sce
* To compare the runs from different compaction times, go to data/compaction_tests and run compaction compare
* In the data folder, there are three series folders (corresponding to the three different H:C ratios). Within each folder there are ten different folders mass 1- 10, which have different starting masses for the volatiles
* Within each massX folder the data is in .dat files. Each row contains iterations in time. The column contents are given by file name. See the main code for definition of contents.
